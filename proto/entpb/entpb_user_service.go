// Code generated by protoc-gen-entgrpc. DO NOT EDIT.
package entpb

import (
	context "context"
	base64 "encoding/base64"
	entproto "entgo.io/contrib/entproto"
	runtime "entgo.io/contrib/entproto/runtime"
	sqlgraph "entgo.io/ent/dialect/sql/sqlgraph"
	fmt "fmt"
	ent "go.phucam.tv/ent"
	account "go.phucam.tv/ent/account"
	user "go.phucam.tv/ent/user"
	useremail "go.phucam.tv/ent/useremail"
	userpassword "go.phucam.tv/ent/userpassword"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
	strconv "strconv"
)

// UserService implements UserServiceServer
type UserService struct {
	client *ent.Client
	UnimplementedUserServiceServer
}

// NewUserService returns a new UserService
func NewUserService(client *ent.Client) *UserService {
	return &UserService{
		client: client,
	}
}

// toProtoUser transforms the ent type to the pb type
func toProtoUser(e *ent.User) (*User, error) {
	v := &User{}
	createdAt := timestamppb.New(e.CreatedAt)
	v.CreatedAt = createdAt
	id := int64(e.ID)
	v.Id = id
	isActive := e.IsActive
	v.IsActive = isActive
	updatedAt := timestamppb.New(e.UpdatedAt)
	v.UpdatedAt = updatedAt
	for _, edg := range e.Edges.Accounts {
		id := int64(edg.ID)
		v.Accounts = append(v.Accounts, &Account{
			Id: id,
		})
	}
	for _, edg := range e.Edges.Emails {
		id := int64(edg.ID)
		v.Emails = append(v.Emails, &UserEmail{
			Id: id,
		})
	}
	if edg := e.Edges.Password; edg != nil {
		id := int64(edg.ID)
		v.Password = &UserPassword{
			Id: id,
		}
	}
	return v, nil
}

// toProtoUserList transforms a list of ent type to a list of pb type
func toProtoUserList(e []*ent.User) ([]*User, error) {
	var pbList []*User
	for _, entEntity := range e {
		pbEntity, err := toProtoUser(entEntity)
		if err != nil {
			return nil, status.Errorf(codes.Internal, "internal error: %s", err)
		}
		pbList = append(pbList, pbEntity)
	}
	return pbList, nil
}

// Create implements UserServiceServer.Create
func (svc *UserService) Create(ctx context.Context, req *CreateUserRequest) (*User, error) {
	user := req.GetUser()
	m, err := svc.createBuilder(user)
	if err != nil {
		return nil, err
	}
	res, err := m.Save(ctx)
	switch {
	case err == nil:
		proto, err := toProtoUser(res)
		if err != nil {
			return nil, status.Errorf(codes.Internal, "internal error: %s", err)
		}
		return proto, nil
	case sqlgraph.IsUniqueConstraintError(err):
		return nil, status.Errorf(codes.AlreadyExists, "already exists: %s", err)
	case ent.IsConstraintError(err):
		return nil, status.Errorf(codes.InvalidArgument, "invalid argument: %s", err)
	default:
		return nil, status.Errorf(codes.Internal, "internal error: %s", err)
	}

}

// Get implements UserServiceServer.Get
func (svc *UserService) Get(ctx context.Context, req *GetUserRequest) (*User, error) {
	var (
		err error
		get *ent.User
	)
	id := int(req.GetId())
	switch req.GetView() {
	case GetUserRequest_VIEW_UNSPECIFIED, GetUserRequest_BASIC:
		get, err = svc.client.User.Get(ctx, id)
	case GetUserRequest_WITH_EDGE_IDS:
		get, err = svc.client.User.Query().
			Where(user.ID(id)).
			WithAccounts(func(query *ent.AccountQuery) {
				query.Select(account.FieldID)
			}).
			WithEmails(func(query *ent.UserEmailQuery) {
				query.Select(useremail.FieldID)
			}).
			WithPassword(func(query *ent.UserPasswordQuery) {
				query.Select(userpassword.FieldID)
			}).
			Only(ctx)
	default:
		return nil, status.Error(codes.InvalidArgument, "invalid argument: unknown view")
	}
	switch {
	case err == nil:
		return toProtoUser(get)
	case ent.IsNotFound(err):
		return nil, status.Errorf(codes.NotFound, "not found: %s", err)
	default:
		return nil, status.Errorf(codes.Internal, "internal error: %s", err)
	}

}

// Update implements UserServiceServer.Update
func (svc *UserService) Update(ctx context.Context, req *UpdateUserRequest) (*User, error) {
	user := req.GetUser()
	userID := int(user.GetId())
	m := svc.client.User.UpdateOneID(userID)
	userIsActive := user.GetIsActive()
	m.SetIsActive(userIsActive)
	for _, item := range user.GetAccounts() {
		accounts := int(item.GetId())
		m.AddAccountIDs(accounts)
	}
	for _, item := range user.GetEmails() {
		emails := int(item.GetId())
		m.AddEmailIDs(emails)
	}
	if user.GetPassword() != nil {
		userPassword := int(user.GetPassword().GetId())
		m.SetPasswordID(userPassword)
	}

	res, err := m.Save(ctx)
	switch {
	case err == nil:
		proto, err := toProtoUser(res)
		if err != nil {
			return nil, status.Errorf(codes.Internal, "internal error: %s", err)
		}
		return proto, nil
	case sqlgraph.IsUniqueConstraintError(err):
		return nil, status.Errorf(codes.AlreadyExists, "already exists: %s", err)
	case ent.IsConstraintError(err):
		return nil, status.Errorf(codes.InvalidArgument, "invalid argument: %s", err)
	default:
		return nil, status.Errorf(codes.Internal, "internal error: %s", err)
	}

}

// Delete implements UserServiceServer.Delete
func (svc *UserService) Delete(ctx context.Context, req *DeleteUserRequest) (*emptypb.Empty, error) {
	var err error
	id := int(req.GetId())
	err = svc.client.User.DeleteOneID(id).Exec(ctx)
	switch {
	case err == nil:
		return &emptypb.Empty{}, nil
	case ent.IsNotFound(err):
		return nil, status.Errorf(codes.NotFound, "not found: %s", err)
	default:
		return nil, status.Errorf(codes.Internal, "internal error: %s", err)
	}

}

// List implements UserServiceServer.List
func (svc *UserService) List(ctx context.Context, req *ListUserRequest) (*ListUserResponse, error) {
	var (
		err      error
		entList  []*ent.User
		pageSize int
	)
	pageSize = int(req.GetPageSize())
	switch {
	case pageSize < 0:
		return nil, status.Errorf(codes.InvalidArgument, "page size cannot be less than zero")
	case pageSize == 0 || pageSize > entproto.MaxPageSize:
		pageSize = entproto.MaxPageSize
	}
	listQuery := svc.client.User.Query().
		Order(ent.Desc(user.FieldID)).
		Limit(pageSize + 1)
	if req.GetPageToken() != "" {
		bytes, err := base64.StdEncoding.DecodeString(req.PageToken)
		if err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "page token is invalid")
		}
		token, err := strconv.ParseInt(string(bytes), 10, 32)
		if err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "page token is invalid")
		}
		pageToken := int(token)
		listQuery = listQuery.
			Where(user.IDLTE(pageToken))
	}
	switch req.GetView() {
	case ListUserRequest_VIEW_UNSPECIFIED, ListUserRequest_BASIC:
		entList, err = listQuery.All(ctx)
	case ListUserRequest_WITH_EDGE_IDS:
		entList, err = listQuery.
			WithAccounts(func(query *ent.AccountQuery) {
				query.Select(account.FieldID)
			}).
			WithEmails(func(query *ent.UserEmailQuery) {
				query.Select(useremail.FieldID)
			}).
			WithPassword(func(query *ent.UserPasswordQuery) {
				query.Select(userpassword.FieldID)
			}).
			All(ctx)
	}
	switch {
	case err == nil:
		var nextPageToken string
		if len(entList) == pageSize+1 {
			nextPageToken = base64.StdEncoding.EncodeToString(
				[]byte(fmt.Sprintf("%v", entList[len(entList)-1].ID)))
			entList = entList[:len(entList)-1]
		}
		protoList, err := toProtoUserList(entList)
		if err != nil {
			return nil, status.Errorf(codes.Internal, "internal error: %s", err)
		}
		return &ListUserResponse{
			UserList:      protoList,
			NextPageToken: nextPageToken,
		}, nil
	default:
		return nil, status.Errorf(codes.Internal, "internal error: %s", err)
	}

}

// BatchCreate implements UserServiceServer.BatchCreate
func (svc *UserService) BatchCreate(ctx context.Context, req *BatchCreateUsersRequest) (*BatchCreateUsersResponse, error) {
	requests := req.GetRequests()
	if len(requests) > entproto.MaxBatchCreateSize {
		return nil, status.Errorf(codes.InvalidArgument, "batch size cannot be greater than %d", entproto.MaxBatchCreateSize)
	}
	bulk := make([]*ent.UserCreate, len(requests))
	for i, req := range requests {
		user := req.GetUser()
		var err error
		bulk[i], err = svc.createBuilder(user)
		if err != nil {
			return nil, err
		}
	}
	res, err := svc.client.User.CreateBulk(bulk...).Save(ctx)
	switch {
	case err == nil:
		protoList, err := toProtoUserList(res)
		if err != nil {
			return nil, status.Errorf(codes.Internal, "internal error: %s", err)
		}
		return &BatchCreateUsersResponse{
			Users: protoList,
		}, nil
	case sqlgraph.IsUniqueConstraintError(err):
		return nil, status.Errorf(codes.AlreadyExists, "already exists: %s", err)
	case ent.IsConstraintError(err):
		return nil, status.Errorf(codes.InvalidArgument, "invalid argument: %s", err)
	default:
		return nil, status.Errorf(codes.Internal, "internal error: %s", err)
	}

}

func (svc *UserService) createBuilder(user *User) (*ent.UserCreate, error) {
	m := svc.client.User.Create()
	userCreatedAt := runtime.ExtractTime(user.GetCreatedAt())
	m.SetCreatedAt(userCreatedAt)
	userIsActive := user.GetIsActive()
	m.SetIsActive(userIsActive)
	userUpdatedAt := runtime.ExtractTime(user.GetUpdatedAt())
	m.SetUpdatedAt(userUpdatedAt)
	for _, item := range user.GetAccounts() {
		accounts := int(item.GetId())
		m.AddAccountIDs(accounts)
	}
	for _, item := range user.GetEmails() {
		emails := int(item.GetId())
		m.AddEmailIDs(emails)
	}
	if user.GetPassword() != nil {
		userPassword := int(user.GetPassword().GetId())
		m.SetPasswordID(userPassword)
	}
	return m, nil
}
