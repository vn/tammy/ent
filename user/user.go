// Code generated by ent, DO NOT EDIT.

package user

import (
	"time"
)

const (
	// Label holds the string label denoting the user type in the database.
	Label = "user"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldIsActive holds the string denoting the isactive field in the database.
	FieldIsActive = "is_active"
	// FieldCreatedAt holds the string denoting the createdat field in the database.
	FieldCreatedAt = "created_at"
	// FieldUpdatedAt holds the string denoting the updatedat field in the database.
	FieldUpdatedAt = "updated_at"
	// EdgePassword holds the string denoting the password edge name in mutations.
	EdgePassword = "password"
	// EdgeAccounts holds the string denoting the accounts edge name in mutations.
	EdgeAccounts = "accounts"
	// EdgeEmails holds the string denoting the emails edge name in mutations.
	EdgeEmails = "emails"
	// Table holds the table name of the user in the database.
	Table = "users"
	// PasswordTable is the table that holds the password relation/edge.
	PasswordTable = "user_passwords"
	// PasswordInverseTable is the table name for the UserPassword entity.
	// It exists in this package in order to avoid circular dependency with the "userpassword" package.
	PasswordInverseTable = "user_passwords"
	// PasswordColumn is the table column denoting the password relation/edge.
	PasswordColumn = "user_password"
	// AccountsTable is the table that holds the accounts relation/edge.
	AccountsTable = "accounts"
	// AccountsInverseTable is the table name for the Account entity.
	// It exists in this package in order to avoid circular dependency with the "account" package.
	AccountsInverseTable = "accounts"
	// AccountsColumn is the table column denoting the accounts relation/edge.
	AccountsColumn = "user_accounts"
	// EmailsTable is the table that holds the emails relation/edge.
	EmailsTable = "user_emails"
	// EmailsInverseTable is the table name for the UserEmail entity.
	// It exists in this package in order to avoid circular dependency with the "useremail" package.
	EmailsInverseTable = "user_emails"
	// EmailsColumn is the table column denoting the emails relation/edge.
	EmailsColumn = "user_emails"
)

// Columns holds all SQL columns for user fields.
var Columns = []string{
	FieldID,
	FieldIsActive,
	FieldCreatedAt,
	FieldUpdatedAt,
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	return false
}

var (
	// DefaultIsActive holds the default value on creation for the "isActive" field.
	DefaultIsActive bool
	// DefaultCreatedAt holds the default value on creation for the "createdAt" field.
	DefaultCreatedAt func() time.Time
	// DefaultUpdatedAt holds the default value on creation for the "updatedAt" field.
	DefaultUpdatedAt func() time.Time
)
