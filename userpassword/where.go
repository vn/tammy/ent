// Code generated by ent, DO NOT EDIT.

package userpassword

import (
	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"go.phucam.tv/ent/predicate"
)

// ID filters vertices based on their ID field.
func ID(id int) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDEQ applies the EQ predicate on the ID field.
func IDEQ(id int) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDNEQ applies the NEQ predicate on the ID field.
func IDNEQ(id int) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldID), id))
	})
}

// IDIn applies the In predicate on the ID field.
func IDIn(ids ...int) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		v := make([]interface{}, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.In(s.C(FieldID), v...))
	})
}

// IDNotIn applies the NotIn predicate on the ID field.
func IDNotIn(ids ...int) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		v := make([]interface{}, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.NotIn(s.C(FieldID), v...))
	})
}

// IDGT applies the GT predicate on the ID field.
func IDGT(id int) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldID), id))
	})
}

// IDGTE applies the GTE predicate on the ID field.
func IDGTE(id int) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldID), id))
	})
}

// IDLT applies the LT predicate on the ID field.
func IDLT(id int) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldID), id))
	})
}

// IDLTE applies the LTE predicate on the ID field.
func IDLTE(id int) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldID), id))
	})
}

// HashedValue applies equality check predicate on the "hashedValue" field. It's identical to HashedValueEQ.
func HashedValue(v string) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldHashedValue), v))
	})
}

// IsActive applies equality check predicate on the "isActive" field. It's identical to IsActiveEQ.
func IsActive(v bool) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldIsActive), v))
	})
}

// CreatedAt applies equality check predicate on the "createdAt" field. It's identical to CreatedAtEQ.
func CreatedAt(v uint32) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldCreatedAt), v))
	})
}

// HashedValueEQ applies the EQ predicate on the "hashedValue" field.
func HashedValueEQ(v string) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldHashedValue), v))
	})
}

// HashedValueNEQ applies the NEQ predicate on the "hashedValue" field.
func HashedValueNEQ(v string) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldHashedValue), v))
	})
}

// HashedValueIn applies the In predicate on the "hashedValue" field.
func HashedValueIn(vs ...string) predicate.UserPassword {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.UserPassword(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldHashedValue), v...))
	})
}

// HashedValueNotIn applies the NotIn predicate on the "hashedValue" field.
func HashedValueNotIn(vs ...string) predicate.UserPassword {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.UserPassword(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldHashedValue), v...))
	})
}

// HashedValueGT applies the GT predicate on the "hashedValue" field.
func HashedValueGT(v string) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldHashedValue), v))
	})
}

// HashedValueGTE applies the GTE predicate on the "hashedValue" field.
func HashedValueGTE(v string) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldHashedValue), v))
	})
}

// HashedValueLT applies the LT predicate on the "hashedValue" field.
func HashedValueLT(v string) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldHashedValue), v))
	})
}

// HashedValueLTE applies the LTE predicate on the "hashedValue" field.
func HashedValueLTE(v string) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldHashedValue), v))
	})
}

// HashedValueContains applies the Contains predicate on the "hashedValue" field.
func HashedValueContains(v string) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldHashedValue), v))
	})
}

// HashedValueHasPrefix applies the HasPrefix predicate on the "hashedValue" field.
func HashedValueHasPrefix(v string) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldHashedValue), v))
	})
}

// HashedValueHasSuffix applies the HasSuffix predicate on the "hashedValue" field.
func HashedValueHasSuffix(v string) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldHashedValue), v))
	})
}

// HashedValueEqualFold applies the EqualFold predicate on the "hashedValue" field.
func HashedValueEqualFold(v string) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldHashedValue), v))
	})
}

// HashedValueContainsFold applies the ContainsFold predicate on the "hashedValue" field.
func HashedValueContainsFold(v string) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldHashedValue), v))
	})
}

// IsActiveEQ applies the EQ predicate on the "isActive" field.
func IsActiveEQ(v bool) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldIsActive), v))
	})
}

// IsActiveNEQ applies the NEQ predicate on the "isActive" field.
func IsActiveNEQ(v bool) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldIsActive), v))
	})
}

// CreatedAtEQ applies the EQ predicate on the "createdAt" field.
func CreatedAtEQ(v uint32) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtNEQ applies the NEQ predicate on the "createdAt" field.
func CreatedAtNEQ(v uint32) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtIn applies the In predicate on the "createdAt" field.
func CreatedAtIn(vs ...uint32) predicate.UserPassword {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.UserPassword(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldCreatedAt), v...))
	})
}

// CreatedAtNotIn applies the NotIn predicate on the "createdAt" field.
func CreatedAtNotIn(vs ...uint32) predicate.UserPassword {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.UserPassword(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldCreatedAt), v...))
	})
}

// CreatedAtGT applies the GT predicate on the "createdAt" field.
func CreatedAtGT(v uint32) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtGTE applies the GTE predicate on the "createdAt" field.
func CreatedAtGTE(v uint32) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtLT applies the LT predicate on the "createdAt" field.
func CreatedAtLT(v uint32) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtLTE applies the LTE predicate on the "createdAt" field.
func CreatedAtLTE(v uint32) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldCreatedAt), v))
	})
}

// HasUser applies the HasEdge predicate on the "user" edge.
func HasUser() predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.To(UserTable, FieldID),
			sqlgraph.Edge(sqlgraph.O2O, true, UserTable, UserColumn),
		)
		sqlgraph.HasNeighbors(s, step)
	})
}

// HasUserWith applies the HasEdge predicate on the "user" edge with a given conditions (other predicates).
func HasUserWith(preds ...predicate.User) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.To(UserInverseTable, FieldID),
			sqlgraph.Edge(sqlgraph.O2O, true, UserTable, UserColumn),
		)
		sqlgraph.HasNeighborsWith(s, step, func(s *sql.Selector) {
			for _, p := range preds {
				p(s)
			}
		})
	})
}

// And groups predicates with the AND operator between them.
func And(predicates ...predicate.UserPassword) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for _, p := range predicates {
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Or groups predicates with the OR operator between them.
func Or(predicates ...predicate.UserPassword) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for i, p := range predicates {
			if i > 0 {
				s1.Or()
			}
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Not applies the not operator on the given predicate.
func Not(p predicate.UserPassword) predicate.UserPassword {
	return predicate.UserPassword(func(s *sql.Selector) {
		p(s.Not())
	})
}
